var config = {
    apiKey: "AIzaSyAqSvupxocwyKdDsZUw2MCiLAwkwxR2RdE",
    authDomain: "flatiron-previewer.firebaseapp.com",
    databaseURL: "https://flatiron-previewer.firebaseio.com",
    projectId: "flatiron-previewer",
    storageBucket: "flatiron-previewer.appspot.com",
    messagingSenderId: "338044716172"
};
firebase.initializeApp(config);
// Initialize Cloud Firestore through Firebase
const settings = {/* your settings... */ timestampsInSnapshots: true};
var db = firebase.firestore();
db.settings(settings);
var app = angular.module("preview", ['ngclipboard']);
var provider = new firebase.auth.GoogleAuthProvider();
if(localStorage.user === undefined) {
    firebase.auth().getRedirectResult().then(function(result) {
        if (result.credential) {
            // This gives you a Google Access Token. You can use it to access the Google API.
            var token = result.credential.accessToken;
            // ...
        }
        // The signed-in user info.
        var user = result.user;
        if(user !== null) {
            localStorage.setItem('user', JSON.stringify(user));
        }
        else {
            firebase.auth().signInWithRedirect(provider);
        }
    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        // The email of the user's account used.
        var email = error.email;
        // The firebase.auth.AuthCredential type that was used.
        var credential = error.credential;
        // ...
    });

}


app.controller("myCtrl", function($scope, $sce, $window) {
    $scope.user = JSON.parse($window.localStorage.getItem('user'));
    $scope.raw = "";
    $scope.search = "";
    $scope.overflow = true;
    $scope.preview = false;
    $scope.trustAsHtml = function() {
        return $sce.trustAsHtml($scope.raw);
    };

    $scope.trust = function(page) {
        return $sce.trustAsHtml($scope.processed(page));
    };

    $scope.processed = function(page) {
        return '<div style="margin: 0 auto; background-color: white;"><style>' + $scope.processor(page) + '</style>' + page.html + '</div>'
    }

    $scope.processor = function(page) {
        re = /([#\.]?[a-z\-]+?\s?{)/i
        console.log(page.style.replace(re, '#container_'+ page.id + ' $1'))
        return page.style.replace(re, '#container_'+ page.id + ' $1')
    }

    $scope.pages = [];
    $window.db.collection("pages").get().then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            page = doc.data();
            page.id = doc.id;
            $scope.pages.push(page);
            $scope.$digest();
        });
    });

    $scope.addTemplate = function() {
        $window.db.collection("pages").add({
            name: $scope.search,
            html: '',
            style: '',
            created_by: $scope.user.displayName,
            created_by_photo: $scope.user.photoURL,
            created_at: new Date()
        }).then(function (docRef) {
            $scope.pages.unshift({
                id: docRef.id,
                name: $scope.search,
                html: '',
                style: '',
                show: true,
                created_by: $scope.user.displayName,
                created_by_photo: $scope.user.photoURL,
                created_at: new Date().toString()
            });
            $scope.$digest();
        })
    }

    $scope.updateTemplate = function(page) {
        $window.db.collection("pages").doc(page.id).set({
            name: page.name,
            html: page.html,
            style: page.style,
            created_by: page.created_by,
            created_by_photo: page.created_by_photo,
            created_at: page.created_at.toString(),
            updated_by: $scope.user.displayName,
            updated_by_photo: $scope.user.photoURL,
            updated_at: new Date().toString()
        }).then(function (docRef) {
            $scope.$digest();
        })
    }

    $scope.deleteTemplate = function(page) {
        $window.db.collection("pages").doc(page.id).delete().then(function (docRef) {
            var index = $scope.pages.indexOf(page);
            $scope.pages.splice(index, 1)
            $scope.$digest();
        })
    }

    $scope.togglePreview = function() {
        $scope.preview = !$scope.preview
    }


});